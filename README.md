# TheShop #

Online shop application with a product catalog, session-based cart, PayPal integration, recommendation engine and discount coupon support

### Features ###

* Session-based cart
* Async email sending with Celery and RabbitMQ
* Paypal Payment Gateway (django-paypal)
* PDF Invoice generated automatically (weasyprint)
* Support for localization and internalization (django-rosetta, django-parler, django-localflavor)
* Recommendation Engine (*'Others also bought...'*) with Redis

### Requirments ###

* Python 3.3+
* requirements.txt
* [Redis 3.0.4](http://redis.io)
* [RabbitMQ](https://www.rabbitmq.com/)