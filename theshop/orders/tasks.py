from celery import task
from django.core.mail import send_mail
from .models import Order


@task
def order_created(order_id):
    """
    send email, when order successfully placed
    """
    order = Order.objects.get(id=order_id)
    subject = "Order no. {}".format(order_id)
    message = "Dear {},\n\nYour order was placed successfully. " \
              "Your order id is {}.".format(order.first_name, order.id)
    mail_sent = send_mail(subject, message, 'admin@theshop.com', [order.email])
    return mail_sent
