import os
from celery import Celery
from django.conf import settings


# default Django settings
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'theshop.settings')

app = Celery('theshop')

app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
